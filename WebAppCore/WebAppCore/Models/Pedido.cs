﻿using System;
using System.Collections.Generic;

namespace WebAppCore
{
    public partial class Pedido
    {
        public decimal Codped { get; set; }
        public decimal? Codcli { get; set; }
        public decimal? Codpro { get; set; }
        public double? Qtde { get; set; }

        public virtual Cliente CodcliNavigation { get; set; }
        public virtual Produto CodproNavigation { get; set; }
    }
}
