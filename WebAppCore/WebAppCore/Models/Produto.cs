﻿using System;
using System.Collections.Generic;

namespace WebAppCore
{
    public partial class Produto
    {
        public Produto()
        {
            Pedido = new HashSet<Pedido>();
        }

        public decimal Codpro { get; set; }
        public string Codigo { get; set; }
        public string Descricao { get; set; }
        public double? Valor { get; set; }

        public virtual ICollection<Pedido> Pedido { get; set; }
    }
}
