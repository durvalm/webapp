﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebAppCore
{
    public partial class BDEXERCICIOContext : DbContext
    {
        public BDEXERCICIOContext()
        {
        }

        public BDEXERCICIOContext(DbContextOptions<BDEXERCICIOContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Cliente> Cliente { get; set; }
        public virtual DbSet<Pedido> Pedido { get; set; }
        public virtual DbSet<Produto> Produto { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
           
                optionsBuilder.UseSqlServer("Password=manager.1;Persist Security Info=True;User ID=sa;Initial Catalog=BDEXERCICIO;Data Source=DFPM-NTB\\SQLEXPRESS");

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cliente>(entity =>
            {
                entity.HasKey(e => e.Codcli);

                entity.ToTable("CLIENTE");

                entity.Property(e => e.Codcli)
                    .HasColumnName("CODCLI")
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Codigo)
                    .IsRequired()
                    .HasColumnName("CODIGO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Endereco)
                    .HasColumnName("ENDERECO")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Nome)
                    .HasColumnName("NOME")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Pedido>(entity =>
            {
                entity.HasKey(e => e.Codped);

                entity.ToTable("PEDIDO");

                entity.Property(e => e.Codped)
                    .HasColumnName("CODPED")
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Codcli)
                    .HasColumnName("CODCLI")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Codpro)
                    .HasColumnName("CODPRO")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Qtde).HasColumnName("QTDE");

                entity.HasOne(d => d.CodcliNavigation)
                    .WithMany(p => p.Pedido)
                    .HasForeignKey(d => d.Codcli)
                    .HasConstraintName("FK_ORDSERV1_CLIEN");

                entity.HasOne(d => d.CodproNavigation)
                    .WithMany(p => p.Pedido)
                    .HasForeignKey(d => d.Codpro)
                    .HasConstraintName("FK_ORDSERV2_PRODU");
            });

            modelBuilder.Entity<Produto>(entity =>
            {
                entity.HasKey(e => e.Codpro);

                entity.ToTable("PRODUTO");

                entity.Property(e => e.Codpro)
                    .HasColumnName("CODPRO")
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Codigo)
                    .IsRequired()
                    .HasColumnName("CODIGO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Descricao)
                    .HasColumnName("DESCRICAO")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Valor).HasColumnName("VALOR");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
