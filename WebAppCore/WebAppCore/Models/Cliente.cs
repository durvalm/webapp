﻿using System;
using System.Collections.Generic;

namespace WebAppCore
{
    public partial class Cliente
    {
        public Cliente()
        {
            Pedido = new HashSet<Pedido>();
        }

        public decimal Codcli { get; set; }
        public string Codigo { get; set; }
        public string Nome { get; set; }
        public string Endereco { get; set; }

        public virtual ICollection<Pedido> Pedido { get; set; }
    }
}
