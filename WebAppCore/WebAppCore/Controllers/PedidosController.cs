﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace WebAppCore.Controllers
{
    public class PedidosController : Controller
    {
        private readonly BDEXERCICIOContext _context;

        public PedidosController(BDEXERCICIOContext context)
        {
            _context = context;
        }

        // GET: Pedidos
        public async Task<IActionResult> Index()
        {
            var bDEXERCICIOContext = _context.Pedido.Include(p => p.CodcliNavigation).Include(p => p.CodproNavigation);
            return View(await bDEXERCICIOContext.ToListAsync());
        }

        // GET: Pedidos/Details/5
        public async Task<IActionResult> Details(decimal? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pedido = await _context.Pedido
                .Include(p => p.CodcliNavigation)
                .Include(p => p.CodproNavigation)
                .FirstOrDefaultAsync(m => m.Codped == id);
            if (pedido == null)
            {
                return NotFound();
            }

            return View(pedido);
        }

        // GET: Pedidos/Create
        public IActionResult Create()
        {
            ViewData["Codcli"] = new SelectList(_context.Cliente, "Codcli", "Codigo");
            ViewData["Codpro"] = new SelectList(_context.Produto, "Codpro", "Codigo");
            return View();
        }

        // POST: Pedidos/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Codped,Codcli,Codpro,Qtde")] Pedido pedido)
        {
            if (ModelState.IsValid)
            {
                _context.Add(pedido);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Codcli"] = new SelectList(_context.Cliente, "Codcli", "Codigo", pedido.Codcli);
            ViewData["Codpro"] = new SelectList(_context.Produto, "Codpro", "Codigo", pedido.Codpro);
            return View(pedido);
        }

        // GET: Pedidos/Edit/5
        public async Task<IActionResult> Edit(decimal? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pedido = await _context.Pedido.FindAsync(id);
            if (pedido == null)
            {
                return NotFound();
            }
            ViewData["Codcli"] = new SelectList(_context.Cliente, "Codcli", "Codigo", pedido.Codcli);
            ViewData["Codpro"] = new SelectList(_context.Produto, "Codpro", "Codigo", pedido.Codpro);
            return View(pedido);
        }

        // POST: Pedidos/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(decimal id, [Bind("Codped,Codcli,Codpro,Qtde")] Pedido pedido)
        {
            if (id != pedido.Codped)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(pedido);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PedidoExists(pedido.Codped))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Codcli"] = new SelectList(_context.Cliente, "Codcli", "Codigo", pedido.Codcli);
            ViewData["Codpro"] = new SelectList(_context.Produto, "Codpro", "Codigo", pedido.Codpro);
            return View(pedido);
        }

        // GET: Pedidos/Delete/5
        public async Task<IActionResult> Delete(decimal? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pedido = await _context.Pedido
                .Include(p => p.CodcliNavigation)
                .Include(p => p.CodproNavigation)
                .FirstOrDefaultAsync(m => m.Codped == id);
            if (pedido == null)
            {
                return NotFound();
            }

            return View(pedido);
        }

        // POST: Pedidos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(decimal id)
        {
            var pedido = await _context.Pedido.FindAsync(id);
            _context.Pedido.Remove(pedido);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PedidoExists(decimal id)
        {
            return _context.Pedido.Any(e => e.Codped == id);
        }
    }
}
