﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class Cliente
    {
        [Key]
        public int ClienteID { get; set; }
        public string Nome { get; set; }
        public string Endereco { get; set; }
        /*public virtual List<Pedido> Pedido { get; set; }*/
    }
}

