﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class Pedido
    {        

        [Key]
        public int PedidoID { get; set; }
        public int ClienteID { get; set; }
        public int ProdutoID { get; set; }
        public float Quantidade { get; set; }
        
        public virtual Cliente ClienteIDNavigation { get; set; }
        public virtual Produto ProdutoIDNavigation { get; set; }
    }
    
}

