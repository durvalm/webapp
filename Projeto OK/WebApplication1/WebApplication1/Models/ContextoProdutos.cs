﻿using System.Data.Entity;

namespace WebApplication1.Models
{
    public class ContextoProdutos : DbContext
    {
        public DbSet<Produto> Produtos { get; set; }
    }
}