﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class Produto
    {
        
        [Key]
        public int ProdutoID { get; set; }
        public string Descricao { get; set; }
        public float Valor { get; set; }

       /* public virtual List<Pedido> Pedido { get; set; }*/

    }
}

